import React from "react";
import { Component } from "react";
import "../../css/styles.css"; 
class ShoppingList extends Component {
  state = {
    items: [],
    newItemName: "",
    isAddingItem: false,
    isEditing: false
  };

  handleAddButtonClick = () => {
    this.setState((state) => {
      return {
        ...state,
        isAddingItem: true,
      };
    });
  };

  handleInputChange = (event) => {
    this.setState((state) => {
      return {
        ...state,
        newItemName: event.target.value,
      };
    });
  };
  handleCancel = () => {
    this.setState((state) => ({
      items: state.items.map((item) => {
        if (item.isEditing) {
          return { ...state, isEditing: false, name: item.newItemName };
        }
        else {
        return item;
        }
      }),
    }));
  };

  handleSaveButtonClick = () => {
    const { items, newItemName } = this.state;
    const newId = items.length + 1;
    const newItem = { id: newId, name: newItemName, isBought: false };
    this.setState((state) => ({
      items: [...state.items, newItem],
      newItemName: "",
      isAddingItem: false,
    }));
  };

  handleDeleteButtonClick = (itemId) => {
    const { items } = this.state;
    const newItems = items.filter((item) => item.id !== itemId);
    this.setState({ items: newItems });
  };

  handleBuyButtonClick = (itemId) => {
    const { items } = this.state;
    const itemIndex = items.findIndex((item) => item.id === itemId);
    const itemToBuy = items[itemIndex];
    const newItems = [...items];
    newItems[itemIndex] = { ...itemToBuy, isBought: true };
    this.setState({ items: newItems });
  };

  handleEditButtonClick = (itemId) => {
    this.setState((state) => ({
      items: state.items.map((item) =>
        item.id === itemId? { ...item, isEditing: true, newItemName: item.name } : item
      )
    }));
  };

  handleEditSaveButtonClick = (itemId, newName) => {
    const { items } = this.state;
    const itemIndex = items.findIndex((item) => item.id === itemId);
    const itemToEdit = items[itemIndex];
    const newItems = [...items];
    newItems[itemIndex] = {
      ...itemToEdit,
      newItemName: newName,
      isEditing: false,
    };
    this.setState({ items: newItems });
  };

  handleEditInputChange = (itemId, value) => {
    this.setState((state) => ({
      items: state.items.map((item) =>
        item.id === itemId ? { ...item, name: value } : item
      ),
    }));
  };

  render() {
    return (
      <>
        <div>
          <h1 className="list-header">Shopping List</h1>
          {this.state.isAddingItem ? (
            <div>
              <input type="text" value={this.state.newItemName} onChange={this.handleInputChange}/>
              <button onClick={this.handleSaveButtonClick}>Save📀</button>
            </div>
          ) : (
            <button className="btn-add-item" onClick={this.handleAddButtonClick}>➕Add New</button>
          )}
        </div>
        
          {this.state.items.map((item) => (
            <div  key={item.id}>
              {item.isEditing ? (
                <div className="box-edit">
                  <input type="text" value={item.name} onChange={(e) => this.handleEditInputChange(item.id, e.target.value)}/>
                  <button className="save-btn" onClick={() => this.handleEditSaveButtonClick(item.id, item.newName)}>
                    Save
                  </button>
                  <button onClick={() => this.handleCancel()}>Cancel</button>
                </div>
              ) : (
                <div className="box-main-state">
                  <span style={{textDecoration: item.isBought ? "line-through" : "none"}}>
                    {item.name}
                  </span>
                  <button className="btn-edit" onClick={() => this.handleEditButtonClick(item.id)}>Edit✍️</button>
                  <button onClick={() => this.handleDeleteButtonClick(item.id)}>Delete❌</button>
                  <button onClick={() => this.handleBuyButtonClick(item.id)}>Done✅</button>
                </div>
              )}
            </div>
          ))}
        
      </>
    );
  }
}
export default ShoppingList



